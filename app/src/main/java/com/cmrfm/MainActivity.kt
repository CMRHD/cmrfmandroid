package com.cmrfm

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.AnimationDrawable
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.PowerManager
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import java.io.IOException


class MainActivity : AppCompatActivity() {


    private val bitRateKey = "KEY_BIT_RATE"
    private val fmUrlKEY = "KEY_URL"
    private val fmKey = "KEY_FM"
    private val wifiKey = "KEY_WIFI"
    private lateinit var playBtn: ImageView
    private lateinit var nextBtn: ImageView
    private lateinit var prevBtn: ImageView

    private lateinit var cmrfm: ImageView
    private lateinit var tamilfm: ImageView
    private lateinit var hindifm: ImageView
    private lateinit var punjabifm: ImageView
    private lateinit var malayalamfm: ImageView

    private lateinit var playingfm: ImageView
    private lateinit var animationView: ImageView

    private lateinit var setting: ImageView
    private lateinit var radioLayout: LinearLayout
    private lateinit var settingLayout: LinearLayout
    private lateinit var settingsBack: ImageView

    private lateinit var mediaPlayer: MediaPlayer

    private lateinit var radioAnimation: AnimationDrawable

    private lateinit var fmSharedPref: SharedPreferences

    private lateinit var tvCmrFm: TextView
    private lateinit var tvTamilFm: TextView
    private lateinit var tvHindiFm: TextView
    private lateinit var tvPunjabiFm: TextView
    private lateinit var tvMalayalamFm: TextView

    private lateinit var ivCmrFm: ImageView
    private lateinit var ivTamilFm: ImageView
    private lateinit var ivHindiFm: ImageView
    private lateinit var ivPunjabiFm: ImageView
    private lateinit var ivMalayalamFm: ImageView

    private lateinit var tvLowBit: TextView
    private lateinit var tvMediumBit: TextView
    private lateinit var tvHighBit: TextView

    private lateinit var ivLowBit: ImageView
    private lateinit var ivMediumBit: ImageView
    private lateinit var ivHighBit: ImageView

    private lateinit var audioUrl: String

    private lateinit var switch_wifi: SwitchCompat
    private lateinit var wifiLock: WifiManager.WifiLock

    private lateinit var station: String

    private var isSettingLayout = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setting = findViewById(R.id.iv_settings)
        radioLayout = findViewById(R.id.radio_layout)
        settingLayout = findViewById(R.id.ly_settings)
        settingsBack = findViewById(R.id.iv_back)


        playingfm = findViewById(R.id.iv_radioLang_play)
        playBtn = findViewById(R.id.play)
        nextBtn = findViewById(R.id.iv_next)
        prevBtn = findViewById(R.id.iv_previous)
        animationView = findViewById(R.id.iv_animation)
        animationView.setBackgroundResource(R.drawable.radio_play);
        radioAnimation = animationView.background as AnimationDrawable

        cmrfm = findViewById(R.id.iv_cmr)
        tamilfm = findViewById(R.id.iv_tamil)
        hindifm = findViewById(R.id.iv_hindi)
        punjabifm = findViewById(R.id.iv_punjabi)
        malayalamfm = findViewById(R.id.iv_malayalam)


        tvCmrFm = findViewById(R.id.tv_cmr_fm)
        tvTamilFm = findViewById(R.id.tv_tamil_fm)
        tvHindiFm = findViewById(R.id.tv_hindi_fm)
        tvPunjabiFm = findViewById(R.id.tv_punjabi_fm)
        tvMalayalamFm = findViewById(R.id.tv_malayalam_fm)

        ivCmrFm = findViewById(R.id.iv_cmr_fm)
        ivTamilFm = findViewById(R.id.iv_tamil_fm)
        ivHindiFm = findViewById(R.id.iv_hindi_fm)
        ivPunjabiFm = findViewById(R.id.iv_punjabi_fm)
        ivMalayalamFm = findViewById(R.id.iv_malayalam_fm)


        tvLowBit = findViewById(R.id.tv_low_bit)
        tvMediumBit = findViewById(R.id.tv_medium_bit)
        tvHighBit = findViewById(R.id.tv_high_bit)

        ivLowBit = findViewById(R.id.iv_low_bit)
        ivMediumBit = findViewById(R.id.iv_medium_bit)
        ivHighBit = findViewById(R.id.iv_high_bit)

        switch_wifi = findViewById(R.id.switch_wifi)



        fmSharedPref = getSharedPreferences("com.cmrfm", Context.MODE_PRIVATE)
        audioUrl =
            fmSharedPref.getString(fmUrlKEY, "https://live.cmr24.net/CMR/TamilHD-MQ/icecast.audio")
                .toString()
        station = fmSharedPref.getString(fmKey, "TAMIL").toString()
        switch_wifi.isChecked = fmSharedPref.getBoolean(wifiKey, false)

        switch_wifi.setOnCheckedChangeListener { compoundButton: CompoundButton, b: Boolean ->
            if (b) {
                val editor: SharedPreferences.Editor = fmSharedPref.edit()
                editor.putBoolean(wifiKey, true)
                editor.apply()
                editor.commit()
            } else {
                val editor: SharedPreferences.Editor = fmSharedPref.edit()
                editor.putBoolean(wifiKey, false)
                editor.apply()
                editor.commit()
            }
        }

        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "myLock")

        wifiLock.acquire()

        mediaPlayer = MediaPlayer()
        bitRateUiUpdate()
        fmUiUpdate()
        playFm()
        startService()
        playBtn.setOnClickListener {
            playAudio()
        }
        nextBtn.setOnClickListener {
            playNext()
        }

        prevBtn.setOnClickListener {
            playPrevious()
        }

        cmrfm.setOnClickListener {
            cmrFmOnclick()
        }

        tamilfm.setOnClickListener {
            tamilFmOnClick()
        }

        hindifm.setOnClickListener {
            hindiFmOnClick()

        }

        punjabifm.setOnClickListener {
            punjabiFmOnClick()
        }

        malayalamfm.setOnClickListener {
            malayalamFmOnClick()
        }

        setting.setOnClickListener {
            isSettingLayout = true
            radioLayout.visibility = View.GONE
            settingLayout.visibility = View.VISIBLE
        }

        settingsBack.setOnClickListener {
            isSettingLayout = false
            radioLayout.visibility = View.VISIBLE
            settingLayout.visibility = View.GONE
        }

        tvLowBit.setOnClickListener {
            editAudioUrlPreference(fmSharedPref.getString(fmKey, "TAMIL").toString(), "Low")
            ivLowBit.visibility = View.VISIBLE
            ivHighBit.visibility = View.INVISIBLE
            ivMediumBit.visibility = View.INVISIBLE
            playFm()
        }

        tvHighBit.setOnClickListener {
            editAudioUrlPreference(fmSharedPref.getString(fmKey, "TAMIL").toString(), "High")
            ivLowBit.visibility = View.INVISIBLE
            ivHighBit.visibility = View.VISIBLE
            ivMediumBit.visibility = View.INVISIBLE
            playFm()
        }


        tvMediumBit.setOnClickListener {
            editAudioUrlPreference(fmSharedPref.getString(fmKey, "TAMIL").toString(), "Medium")
            ivLowBit.visibility = View.INVISIBLE
            ivHighBit.visibility = View.INVISIBLE
            ivMediumBit.visibility = View.VISIBLE
            playFm()
        }


        tvCmrFm.setOnClickListener {
            editAudioUrlPreference("CMR", fmSharedPref.getString(bitRateKey, "Medium").toString())
            cmrSettingUpdate()
            playFm()
        }

        tvTamilFm.setOnClickListener {
            editAudioUrlPreference("TAMIL", fmSharedPref.getString(bitRateKey, "Medium").toString())
            tamilSettingUpdate()
            playFm()
        }

        tvHindiFm.setOnClickListener {
            editAudioUrlPreference("HINDI", fmSharedPref.getString(bitRateKey, "Medium").toString())
            hindiSettingUpdate()
            playFm()
        }

        tvPunjabiFm.setOnClickListener {
            editAudioUrlPreference(
                "PUNJABI",
                fmSharedPref.getString(bitRateKey, "Medium").toString()
            )
            punjabiSettingUpdate()
            playFm()
        }
        tvMalayalamFm.setOnClickListener {
            editAudioUrlPreference(
                "MALAYALAM", fmSharedPref.getString(
                    bitRateKey,
                    "Medium"
                ).toString()
            )
            malayalamSettingUpdate()
            playFm()
        }

    }

    private fun malayalamSettingUpdate() {
        ivCmrFm.visibility = View.INVISIBLE
        ivTamilFm.visibility = View.INVISIBLE
        ivHindiFm.visibility = View.INVISIBLE
        ivPunjabiFm.visibility = View.INVISIBLE
        ivMalayalamFm.visibility = View.VISIBLE
    }

    private fun punjabiSettingUpdate() {
        ivCmrFm.visibility = View.INVISIBLE
        ivTamilFm.visibility = View.INVISIBLE
        ivHindiFm.visibility = View.INVISIBLE
        ivPunjabiFm.visibility = View.VISIBLE
        ivMalayalamFm.visibility = View.INVISIBLE
    }

    private fun hindiSettingUpdate() {
        ivCmrFm.visibility = View.INVISIBLE
        ivTamilFm.visibility = View.INVISIBLE
        ivHindiFm.visibility = View.VISIBLE
        ivPunjabiFm.visibility = View.INVISIBLE
        ivMalayalamFm.visibility = View.INVISIBLE
    }

    private fun tamilSettingUpdate() {
        ivCmrFm.visibility = View.INVISIBLE
        ivTamilFm.visibility = View.VISIBLE
        ivHindiFm.visibility = View.INVISIBLE
        ivPunjabiFm.visibility = View.INVISIBLE
        ivMalayalamFm.visibility = View.INVISIBLE
    }

    private fun cmrSettingUpdate() {
        ivCmrFm.visibility = View.VISIBLE
        ivTamilFm.visibility = View.INVISIBLE
        ivHindiFm.visibility = View.INVISIBLE
        ivPunjabiFm.visibility = View.INVISIBLE
        ivMalayalamFm.visibility = View.INVISIBLE
    }

    private fun playNext() {
        when (station) {
            "CMR" -> tamilFmOnClick()
            "TAMIL" -> hindiFmOnClick()
            "HINDI" -> punjabiFmOnClick()
            "PUNJABI" -> malayalamFmOnClick()
            "MALAYALAM" -> cmrFmOnclick()
        }
    }

    private fun playPrevious() {
        when (station) {
            "CMR" -> malayalamFmOnClick()
            "TAMIL" -> cmrFmOnclick()
            "HINDI" -> tamilFmOnClick()
            "PUNJABI" -> hindiFmOnClick()
            "MALAYALAM" -> punjabiFmOnClick()
        }
    }

    private fun malayalamFmOnClick() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()

        }
        radioAnimation.stop()
        cmrfm.setBackgroundResource(R.drawable.cmr_default)
        tamilfm.setBackgroundResource(R.drawable.tamil_default)
        hindifm.setBackgroundResource(R.drawable.hindi_default)
        punjabifm.setBackgroundResource(R.drawable.punjabi_default)
        malayalamfm.setBackgroundResource(R.drawable.malayalam_selected)
        playingfm.setBackgroundResource(R.drawable.malayalam_asset)
        when (fmSharedPref.getString(bitRateKey, "Medium").toString()) {
            "High" -> audioUrl = "https://live.cmr24.net/CMR/Malayalam-HQ/icecast.audio"
            "Medium" -> audioUrl = "https://live.cmr24.net/CMR/Malayalam-MQ/icecast.audio"
            "Low" -> audioUrl = "https://live.cmr24.net/CMR/Malayalam-LQ/icecast.audio"
        }
        station = "MALAYALAM"
        playAudio()
    }

    private fun punjabiFmOnClick() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
        }
        radioAnimation.stop()
        cmrfm.setBackgroundResource(R.drawable.cmr_default)
        tamilfm.setBackgroundResource(R.drawable.tamil_default)
        hindifm.setBackgroundResource(R.drawable.hindi_default)
        punjabifm.setBackgroundResource(R.drawable.punjabi_selected)
        malayalamfm.setBackgroundResource(R.drawable.malayalam_default)
        playingfm.setBackgroundResource(R.drawable.punjabi_asset)
        when (fmSharedPref.getString(bitRateKey, "Medium").toString()) {
            "High" -> audioUrl = "https://live.cmr24.net/CMR/Punjabi-HQ/icecast.audio"
            "Medium" -> audioUrl = "https://live.cmr24.net/CMR/Punjabi-MQ/icecast.audio"
            "Low" -> audioUrl = "https://live.cmr24.net/CMR/Punjabi-LQ/icecast.audio"
        }
        station = "PUNJABI"
        playAudio()
    }

    private fun hindiFmOnClick() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
        }
        radioAnimation.stop()
        cmrfm.setBackgroundResource(R.drawable.cmr_default)
        tamilfm.setBackgroundResource(R.drawable.tamil_default)
        hindifm.setBackgroundResource(R.drawable.hindi_selected)
        punjabifm.setBackgroundResource(R.drawable.punjabi_default)
        malayalamfm.setBackgroundResource(R.drawable.malayalam_default)
        playingfm.setBackgroundResource(R.drawable.hindi_asset)
        when (fmSharedPref.getString(bitRateKey, "Medium").toString()) {
            "High" -> audioUrl = "https://live.cmr24.net/CMR/Desi_Music-HQ/icecast.audio"
            "Medium" -> audioUrl = "https://live.cmr24.net/CMR/Desi_Music-MQ/icecast.audio"
            "Low" -> audioUrl = "https://live.cmr24.net/CMR/Desi_Music-LQ/icecast.audio"
        }
        playAudio()
        station = "HINDI"
    }

    private fun tamilFmOnClick() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
        }
        radioAnimation.stop()
        cmrfm.setBackgroundResource(R.drawable.cmr_default)
        tamilfm.setBackgroundResource(R.drawable.tamil_selected)
        hindifm.setBackgroundResource(R.drawable.hindi_default)
        punjabifm.setBackgroundResource(R.drawable.punjabi_default)
        malayalamfm.setBackgroundResource(R.drawable.malayalam_default)
        playingfm.setBackgroundResource(R.drawable.tamil_asset)
        when (fmSharedPref.getString(bitRateKey, "Medium").toString()) {
            "High" -> audioUrl = "https://live.cmr24.net/CMR/TamilHD-HQ/icecast.audio"
            "Medium" -> audioUrl = "https://live.cmr24.net/CMR/TamilHD-MQ/icecast.audio"
            "Low" -> audioUrl = "https://live.cmr24.net/CMR/TamilHD-LQ/icecast.audio"
        }
        playAudio()
        station = "TAMIL"
    }

    private fun cmrFmOnclick() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
        }
        radioAnimation.stop()
        cmrfm.setBackgroundResource(R.drawable.cmr_selected)
        tamilfm.setBackgroundResource(R.drawable.tamil_default)
        hindifm.setBackgroundResource(R.drawable.hindi_default)
        punjabifm.setBackgroundResource(R.drawable.punjabi_default)
        malayalamfm.setBackgroundResource(R.drawable.malayalam_default)
        playingfm.setBackgroundResource(R.drawable.cmrfm_asset)
        when (fmSharedPref.getString(bitRateKey, "Medium").toString()) {
            "High" -> audioUrl = "https://live.cmr24.net/CMR/CMR-HQ/icecast.audio"
            "Medium" -> audioUrl = "https://live.cmr24.net/CMR/CMR-MQ/icecast.audio"
            "Low" -> audioUrl = "https://live.cmr24.net/CMR/CMR-LQ/icecast.audio"
        }
        playAudio()
        station = "CMR"
    }


    private fun playFm() {
        when (fmSharedPref.getString(fmKey, "TAMIL").toString()) {
            "CMR" -> cmrFmOnclick()
            "TAMIL" -> tamilFmOnClick()
            "HINDI" -> hindiFmOnClick()
            "PUNJABI" -> punjabiFmOnClick()
            "MALAYALAM" -> malayalamFmOnClick()
        }
    }

    private fun playAudio() {
        if (playWifi()) {
            if (mediaPlayer.isPlaying) {
                mediaPlayer.stop()
                radioAnimation.stop()
                wifiLock.release()
                playBtn.setBackgroundResource(R.drawable.play)
            } else {
                try {
                    mediaPlayer.release()
                    mediaPlayer = MediaPlayer().apply {
                        setAudioAttributes(
                            AudioAttributes.Builder()
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .setUsage(AudioAttributes.USAGE_MEDIA)
                                .build()
                        )
                        setWakeMode(applicationContext, PowerManager.PARTIAL_WAKE_LOCK)
                        setDataSource(audioUrl)
                        prepareAsync()
                        setOnPreparedListener { mp ->
                            mp.start()
                            wifiLock.acquire()
                            playBtn.setBackgroundResource(R.drawable.pause)
                            radioAnimation.start()
                        }

                        setOnErrorListener { mp, what, extra ->
                            Toast.makeText(
                                applicationContext,
                                "Unable to play the channel please try again later",
                                Toast.LENGTH_SHORT
                            ).show()
                            false
                        }

                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(
                        applicationContext,
                        "Unable to play the channel please try again later",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        } else {
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
                radioAnimation.stop()
                wifiLock.release()
                playBtn.setBackgroundResource(R.drawable.play)
            }
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Radio cannot be played with mobile data")
            builder.setMessage("Please uncheck wifi only option in settings screen ")
            builder.setPositiveButton(android.R.string.ok) { dialog, which ->
                dialog.dismiss()
            }

            builder.show()

        }
    }


    private fun editAudioUrlPreference(stationFm: String, bitRate: String) {
        val editor: SharedPreferences.Editor = fmSharedPref.edit()
        editor.putString(fmKey, stationFm)
        editor.putString(bitRateKey, bitRate)
        editor.apply()
        editor.commit()
    }

    private fun bitRateUiUpdate() {
        when (fmSharedPref.getString(bitRateKey, "Medium").toString()) {
            "High" -> {
                ivLowBit.visibility = View.INVISIBLE
                ivHighBit.visibility = View.VISIBLE
                ivMediumBit.visibility = View.INVISIBLE
            }

            "Medium" -> {
                ivLowBit.visibility = View.INVISIBLE
                ivHighBit.visibility = View.INVISIBLE
                ivMediumBit.visibility = View.VISIBLE
            }

            "Low" -> {
                ivLowBit.visibility = View.VISIBLE
                ivHighBit.visibility = View.INVISIBLE
                ivMediumBit.visibility = View.INVISIBLE
            }
        }
    }

    private fun fmUiUpdate() {
        when (fmSharedPref.getString(fmKey, "TAMIL").toString()) {
            "CMR" -> {
                cmrSettingUpdate()
            }

            "TAMIL" -> {
                tamilSettingUpdate()
            }

            "HINDI" -> {
                hindiSettingUpdate()
            }

            "PUNJABI" -> {
                punjabiSettingUpdate()
            }

            "MALAYALAM" -> {
                malayalamSettingUpdate()
            }

        }
    }

    private fun playWifi(): Boolean {
        if (fmSharedPref.getBoolean(wifiKey, false)) {
            val connManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager

            val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

            return mWifi!!.isConnected
        } else {
            return true
        }

    }

    fun startService() {
        val serviceIntent = Intent(this, ForegroundService::class.java)
        serviceIntent.putExtra("inputExtra", "Canadian Multicultural Radio diversity FM 101.3")
        ContextCompat.startForegroundService(this, serviceIntent)
    }

    fun stopService() {
        val serviceIntent = Intent(this, ForegroundService::class.java)
        stopService(serviceIntent)
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.pause()
        stopService()
    }

    override fun onBackPressed() {
        if (isSettingLayout) {
            isSettingLayout = false
            radioLayout.visibility = View.VISIBLE
            settingLayout.visibility = View.GONE
        } else {
            finish()
            super.onBackPressed()
        }
    }

}

